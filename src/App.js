import React, { Component } from "react";
import "./App.css";
import Menu from "./components/Menu";
import { PureComponent } from "react";
import bootstrap from "bootstrap/dist/css/bootstrap.min.css";
import HomePage from "./containers/HomePage";
import NewOrder from "./containers/NewOrder";
import Sales from "./containers/Sales";
import Coffees from "./containers/Coffees";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
  return (
    <div className="container-floid">
      <Menu />
      <Router>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route exact path="/newOrder" component={NewOrder} />
        <Route path="/sales" component={Sales} />
        <Route path="/coffees" component={Coffees} />
      </Router>
    </div>
  );
}

export default App;
