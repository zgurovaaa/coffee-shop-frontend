import axios from "axios";

const API_URL = 'http://localhost:8090';
const ORDER = 'coffee-order';
const ORDER_API_URL = `${API_URL}/${ORDER}`;

class OrderService {
    retrieveAllOrders() {
        return axios.get(`${ORDER_API_URL}`);
    }

    retrieveOrder(id) {
        return axios.get(`${ORDER_API_URL}/${id}`);
    }

    deleteOrder(id) {
        return axios.delete(`${ORDER_API_URL}/${id}`);
    }

    createOrder(type, coffeeId, milkType) {
        return axios.post(`${ORDER_API_URL}`, null, { params : {
            type,
            coffeeId,
            milkType
        }});
    }
}
export default new OrderService();