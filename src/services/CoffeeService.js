import axios from "axios";

const API_URL = 'http://localhost:8090';
const COFFEE = 'coffee';
const COFFEE_API_URL = `${API_URL}/${COFFEE}`;

class CoffeeService {
    retrieveAllCoffees() {
        return axios.get(`${COFFEE_API_URL}`);
    }

    retrieveCoffee(id) {
        return axios.get(`${COFFEE_API_URL}/${id}`);
    }

    createCoffee(coffee) {
        return axios.post(`${COFFEE_API_URL}`, coffee);
    }

    editCoffee(id, coffee) {
        return axios.put(`${COFFEE_API_URL}/${id}`, coffee);
    }

    deleteCoffee(id) {
        return axios.delete(`${COFFEE_API_URL}/${id}`);
    }
}

export default new CoffeeService();