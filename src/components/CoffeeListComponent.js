import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import { Formik, Form, Field, ErrorMessage } from "formik";
import CoffeeService from "../services/CoffeeService";
import "../containers/Coffees.css";

class CoffeeListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coffees: [],
      message: null,
      id: "",
      species: "",
      name: "",
      region: "",
      deliveryPrice: "",
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.refreshCoffees = this.refreshCoffees.bind(this);
    this.deleteCoffee = this.deleteCoffee.bind(this);
  }

  componentDidMount() {
    this.refreshCoffees();
  }

  deleteCoffee(id) {
    CoffeeService.deleteCoffee(id).then((response) => {
      this.setState({ message: `Delete of coffee ${id} Successful` });
      this.refreshCoffees();
    });
  }

  updateCoffee(id) {
    CoffeeService.retrieveCoffee(id).then((response) => {
      this.setState({
        id: response.data.id,
        species: response.data.species,
        name: response.data.name,
        region: response.data.region,
        deliveryPrice: response.data.deliveryPrice,
      });
    });
  }

  refreshCoffees() {
    CoffeeService.retrieveAllCoffees().then((response) => {
      this.setState({ coffees: response.data.coffees });
    });
  }

  onSubmit(values, {resetForm}) {
    let coffee = {
      species: values.species,
      name: values.name,
      region: values.region,
      deliveryPrice: values.deliveryPrice,
    };

    if(values.id != "") {
        CoffeeService.editCoffee(values.id, coffee).then(() => this.refreshCoffees());
    } else {
        CoffeeService.createCoffee(coffee).then(() => this.refreshCoffees());
    }

    resetForm(values);    
  }

  render() {
    return (
      <div className="container">
        <Formik
          initialValues={{
            id: this.state.id,
            species: this.state.species,
            name: this.state.name,
            region: this.state.region,
            deliveryPrice: this.state.deliveryPrice,
          }}
          onSubmit={this.onSubmit}
          validateOnChange={false}
          validateOnBlur={false}
          enableReinitialize={true}
        >
          {(props) => (
            <Form className="coffee-form">
              <fieldset className="form-group" id="id-fieldset">
                <label>Id</label>
                <Field
                  className="form-control"
                  type="text"
                  name="id"
                  disabled
                />
              </fieldset>
              <fieldset className="form-group" id="species-fieldset">
                <label>Species</label>
                <Field className="form-control" as="select" name="species">
                  <option value=""></option>
                  <option value="Arabica">Arabica</option>
                  <option value="Robusta">Robusta</option>
                </Field>
              </fieldset>
              <fieldset className="form-group" id="name-fieldset">
                <label>Name</label>
                <Field className="form-control" type="text" name="name" />
              </fieldset>
              <fieldset className="form-group" id="region-fieldset">
                <label>Region</label>
                <Field className="form-control" type="text" name="region" />
              </fieldset>
              <fieldset className="form-group" id="delivery-price-fieldset">
                <label>Delivery price</label>
                <Field
                  className="form-control"
                  type="text"
                  name="deliveryPrice"
                />
              </fieldset>
              <button className="btn btn-success btn-save" type="submit">
                Save
              </button>
            </Form>
          )}
        </Formik>
        {this.state.message && (
          <div className="alert alert-success">{this.state.message}</div>
        )}
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Species</th>
              <th>Name</th>
              <th>Region</th>
              <th>Delivery price</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.state.coffees.map((coffee) => (
              <tr key={coffee.id}>
                <td>{coffee.id}</td>
                <td>{coffee.species}</td>
                <td>{coffee.name}</td>
                <td>{coffee.region}</td>
                <td id="price-col">{coffee.deliveryPrice}</td>
                <td id="trash-col">
                  <button
                    className="btn"
                    onClick={() => this.deleteCoffee(coffee.id)}
                  >
                    <img id="trash-btn" src="https://img.icons8.com/carbon-copy/100/000000/filled-trash.png"/>
                  </button>
                </td>
                <td id="edit-col">
                  <button
                    className="btn"
                    onClick={() => this.updateCoffee(coffee.id)}
                  >
                    <img id="edit-btn" src="https://img.icons8.com/wired/64/000000/edit.png"/>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    );
  }
}

export default CoffeeListComponent;
