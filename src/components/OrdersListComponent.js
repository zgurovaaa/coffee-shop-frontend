import React, { Component } from "react";
import Table from "react-bootstrap/Table";
import OrderService from "../services/OrderService";
import "../containers/Sales.css";

class OrdersListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      orders: [],
      totalPrice: "",
      profit: "",
      message: null,
    };
    this.deleteOrderClicked = this.deleteOrderClicked.bind(this);
    this.refreshOrders = this.refreshOrders.bind(this);
  }

  componentDidMount() {
    this.refreshOrders();
  }

  refreshOrders() {
    OrderService.retrieveAllOrders().then((response) => {
      this.setState({
        orders: response.data.orders,
        totalPrice: Number(response.data.total).toFixed(2),
        profit: Number(response.data.profit).toFixed(2),
      });
    });
  }

  deleteOrderClicked(id) {
    OrderService.deleteOrder(id).then((response) => {
      this.setState({ message: `Delete of order ${id} successful` });
      this.refreshOrders();
    });
  }
  render() {
    return (
      <div className="container">
        {this.state.message && (
          <div className="alert alert-success">{this.state.message}</div>
        )}
        <Table striped bordered hover size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Coffee drink</th>
              <th>Coffee</th>
              <th>Milk</th>
              <th>Amount</th>
              <th>Price</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {this.state.orders.map((order) => (
              <tr key={order.id}>
                <td>{order.id}</td>
                <td>{order.type}</td>
                <td>{order.coffee.name}</td>
                <td>{order.milk}</td>
                <td>{order.amount} ml.</td>
                <td>{Number(order.marketPrice).toFixed(2)}</td>
                <td id="trash-col">
                  <button
                    className="btn"
                    onClick={() => this.deleteOrderClicked(order.id)}
                  >
                    <img id="trash-btn" src="https://img.icons8.com/carbon-copy/100/000000/filled-trash.png"/>
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
        <p>Total: {this.state.totalPrice}</p>
        <p>Profit: {this.state.profit}</p>
      </div>
    );
  }
}

export default OrdersListComponent;
