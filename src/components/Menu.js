import React, { Component } from "react";
import logo from "../img/logo.png";
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";

function Menu() {
  return (
      <div>
        <Navbar bg="light" expand="lg">
          <Navbar.Brand href="/">
            <img
              src={logo}
              alt="logo"
              width="35"
              height="30"
              className="d-inline-block align-top pr-1 mr-1"
            />
            Home
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="/newOrder" eventKey="/">
                New order
              </Nav.Link>
              <Nav.Link href="/sales" eventKey="/">
                Sales
              </Nav.Link>
              <Nav.Link href="/coffees" eventKey="/">Coffees</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
  );
}

export default Menu;
