import React, { Component } from "react";
import { PureComponent } from "react";
import Select from "react-select";
import { Formik, Form, Field, ErrorMessage } from "formik";
import OrderService from "../services/OrderService";
import CoffeeService from "../services/CoffeeService";
import "./NewOrder.css";
import "../App.css";
import drinks from "../img/image.png";

const price = 0.0;

class NewOrder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      coffeeType: "",
      coffeeId: "",
      milkType: "",
      coffees: [],
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.validate = this.validate.bind(this);
    this.loadCoffees = this.loadCoffees.bind(this);
  }

  componentDidMount() {
    this.loadCoffees();
  }

  loadCoffees() {
    CoffeeService.retrieveAllCoffees().then((response) => {
      this.setState({ coffees: response.data.coffees });
    });
  }

  validate(values) {
    let errors = {};
    if (values.coffeeId === "") {
      errors.coffeeId = "Enter a coffee";
    } 
    if (values.milkType === "" && (values.coffeeType == "Macciato" || values.coffeeType == "Cappucino" || values.coffeeType == "Latte" || values.coffeeType == "IrishCoffee")) {
      errors.milkType = "Enter a milk";
    }
    return errors;
  }

  onSubmit(values) {
    this.validate(values);
    OrderService.createOrder(values.coffeeType, values.coffeeId, values.milkType).then(() =>
      this.props.history.push("/sales")
    );
  }
  render() {
    return (
      <div className="card border-0 shadow m-5">
        <div className="inf-container card-body p5">
          <h1 id="page-title">New order</h1>
          <div className="order-container">
            <img id="drinks-img" src={drinks} alt="Coffee drinks" />
            <div className="order">
              <Formik
                initialValues={{ coffeeType: "Espresso", coffeeId: "", milkType: "" }}
                onSubmit={this.onSubmit}
                validateOnChange={false}
                validateOnBlur={false}
                validate={this.validate}
                enableReinitialize={true}
              >
                {(props) => (
                  <Form>
                    <ErrorMessage
                      name="coffeeId"
                      component="div"
                      className="alert alert-warning"
                    />
                    <ErrorMessage
                      name="milkType"
                      component="div"
                      className="alert alert-warning"
                    />
                    <fieldset className="form-group">
                      <label>Drink</label>
                      <Field
                        className="form-control"
                        as="select"
                        name="coffeeType"
                      >
                        <option value="Espresso">Espresso</option>
                        <option value="Macciato">Macciato</option>
                        <option value="Americano">Americano</option>
                        <option value="Lungo">Lungo</option>
                        <option value="Cappucino">Cappucino</option>
                        <option value="Latte">Latte</option>
                        <option value="Romano">Romano</option>
                        <option value="IrishCoffee">Irish coffee</option>
                      </Field>
                    </fieldset>
                    <fieldset className="form-group">
                      <label>Coffee</label>
                      <Field
                        className="form-control"
                        as="select"
                        name="coffeeId"
                      >
                        <option value=""></option>
                        {this.state.coffees.map((coffee) => (
                          <option key={coffee.id} value={coffee.id}>
                            {coffee.name}
                          </option>
                        ))}
                      </Field>
                    </fieldset>
                    <fieldset className="form-group">
                      <label>Milk</label>
                      <Field
                        className="form-control"
                        as="select"
                        name="milkType"
                      >
                        <option value=""></option>
                        <option value="Milk">Cow milk</option>
                        <option value="Almond">Almond milk</option>
                        <option value="Coconut">Coconut milk</option>
                        <option value="LactoseFree">Lactose free milk</option>
                      </Field>
                    </fieldset>
                    <button className="btn btn-success" type="submit">
                      Save
                    </button>
                  </Form>
                )}
              </Formik>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NewOrder;
