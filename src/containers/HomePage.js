import React, { Component } from "react";
import Carousel from "react-bootstrap/Carousel";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import MapContainer from "../components/MapContainer";
import "./HomePage.css";
import "../App.css";
import logotext from "../img/coffee-cup-cup-of-coffee.png";
import espressoimg from "../img/espresso.png";
import latteimg from "../img/latte.png";
import dalgonaimg from "../img/dalgona-coffee.png";
import cappuccino from "../img/cappuccino-banana.png";
import chailatte from "../img/chai-latte.png";
import coffeeJelly from "../img/coffee-jelly.png";
import icedCoffee from "../img/Iced-Coffee.png";
import pumpkinLatte from "../img/Pumpkin-Spice-Latte-Recipe.png";
import frappucino from "../img/frappucino.png";

function HomePage() {
  return (
    <div className="card border-0 shadow m-5">
      <div className="inf-container card-body p-5">
        <img
          src={logotext}
          alt="logoimg"
          className="mx-auto d-block"
          id="logoImg"
        />
        <Row>
          <Col xs={4} id="colCarousel">
            <Carousel id="firstCarousel" className="shadow m-3">
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={espressoimg}
                  alt="Espresso img"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img className="d-block w-100" src={latteimg} alt="Latte img" />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={dalgonaimg}
                  alt="DalgonaCoffee img"
                />
              </Carousel.Item>
            </Carousel>
            <Carousel id="secondCarousel" className="shadow">
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={cappuccino}
                  alt="Cappuccino img"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={pumpkinLatte}
                  alt="Pumpkin Latte img"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={chailatte}
                  alt="Chai Latte img"
                />
              </Carousel.Item>
            </Carousel>
            <Carousel id="thirdCarousel" className="shadow m-3">
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={coffeeJelly}
                  alt="Jally Coffee img"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={icedCoffee}
                  alt="Iced Coffee img"
                />
              </Carousel.Item>
              <Carousel.Item>
                <img
                  className="d-block w-100"
                  src={frappucino}
                  alt="Frappucino img"
                />
              </Carousel.Item>
            </Carousel>
          </Col>
          {/* <p>ToDo: connection with google maps api</p> */}

          <Col sm>
            <MapContainer />
          </Col>
          <Col sm id="timetable">
            <h4>Work time:</h4>
            <p>Monday: 08:00 - 19:00</p>
            <p>Tuesday: 08:00 - 19:00</p>
            <p>Wednesday: 08:00 - 19:00</p>
            <p>Thursday: 08:00 - 19:00</p>
            <p>Friday: 08:00 - 19:00</p>
            <p>Saturday: 08:00 - 19:00</p>
            <p>Sunday: 08:00 - 19:00</p>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default HomePage;
