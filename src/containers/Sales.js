import React, { Component } from "react";
import OrdersListComponent from "../components/OrdersListComponent";
import "./Sales.css";
import "../App.css";

class Sales extends Component {
  render() {
    return (
      <div className="card border-0 shadow m-5">
        <div className="inf-container card-body p5">
          <h1 id="page-title">Sales</h1>
          <OrdersListComponent />
        </div>
      </div>
    );
  }
}

export default Sales;
