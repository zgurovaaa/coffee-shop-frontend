import React, { Component } from "react";
import CoffeeListComponent from "../components/CoffeeListComponent";
import "./Coffees.css";
import "../App.css";

class Sales extends Component {
  render() {
    return (
      <div className="card border-0 shadow m-5">
        <div className="inf-container card-body p5">
          <h1 id="page-title">Coffees</h1>
          <CoffeeListComponent />
        </div>
      </div>
    );
  }
}

export default Sales;
